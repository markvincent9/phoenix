# Phoenix

## Build Setup

```bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm start

# for development, run auth-service on port 4000

# build for production with minification
npm run build
```

## Docker
To start phoenix and nginx:

```
docker-compose up
```

## Reference
docker compose for nodejs and nginx [example](https://serverfault.com/questions/904131/docker-compose-for-nginx-and-nodejs-server)

How to use TLS with docker [here](https://stackoverflow.com/questions/39846649/how-to-use-lets-encrypt-with-docker-container-based-on-the-node-js-image)