/**
 * Users Routes
 */
/* eslint-disable */
import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

// async components
import {
    AsyncUsersListComponent,
    AsyncUserProfileComponent,
    AsyncUserProfile1Component,
    AsyncUserManagementComponent
} from 'Components/AsyncComponent/AsyncComponent';

import { ADMIN, USER } from 'Constants/roles'
import PrivateRoute from 'Components/Route/PrivateRoute'

const Forms = ({ match }) => (
    <div className="content-wrapper">
        <Switch>
            <Redirect exact from={`${match.url}/`} to={`${match.url}/user-profile`} />
            <PrivateRoute path={`${match.url}/user-profile`} allowedRoles={[ADMIN, USER]} component={AsyncUserProfileComponent} />
            <PrivateRoute path={`${match.url}/user-list`} allowedRoles={[ADMIN]} component={AsyncUsersListComponent} />
            <PrivateRoute path={`${match.url}/user-profile-1`} allowedRoles={[ADMIN, USER]} component={AsyncUserProfile1Component} />
            <PrivateRoute path={`${match.url}/user-management`} allowedRoles={[ADMIN]} component={AsyncUserManagementComponent} />
            <Redirect to="/session/404" />
        </Switch>
    </div>
);

export default Forms;
