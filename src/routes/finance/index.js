/**
 * Finance Routes
 */
/* eslint-disable */
import React from 'react';
import { Redirect, Switch } from 'react-router-dom';

// async components
import {
  AsyncFinanceWithdrawalsListComponent,
  AsyncFinanceDepositsListComponent,
  AsyncFinanceCashAdjustmentsComponent
} from 'Components/AsyncComponent/AsyncComponent';

import { ADMIN } from 'Constants/roles';
import PrivateRoute from 'Components/Route/PrivateRoute';
import 'Components/Finance/finance.css';

const Forms = ({ match }) => (
  <div className="content-wrapper">
    <Switch>
      <PrivateRoute
        path={`${match.url}/withdrawals`}
        allowedRoles={[ADMIN]}
        component={AsyncFinanceWithdrawalsListComponent}
      />
      <PrivateRoute
        path={`${match.url}/deposits`}
        allowedRoles={[ADMIN]}
        component={AsyncFinanceDepositsListComponent}
      />
      <PrivateRoute
        path={`${match.url}/cash-adjustments`}
        allowedRoles={[ADMIN]}
        component={AsyncFinanceCashAdjustmentsComponent}
      />
      <Redirect to="/session/404" />
    </Switch>
  </div>
);

export default Forms;
