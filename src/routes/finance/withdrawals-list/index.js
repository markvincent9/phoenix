import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import { Helmet } from 'react-helmet';
import {
  Button,
  Checkbox,
  FormControl,
  FormControlLabel,
  FormGroup,
  IconButton,
  Input,
  InputLabel,
  Table,
  TableBody,
  TableCell,
  TableFooter,
  TableHead,
  TableRow,
  Tooltip,
  Popover
} from '@material-ui/core';
import { DatePicker } from 'material-ui-pickers';
import { Scrollbars } from 'react-custom-scrollbars';
import { Alert} from 'reactstrap';

import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import IntlMessages from 'Util/IntlMessages';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

import AutoComplete from './autoComplete';
import WitdhrawalConfirmation from './withdrawalConfirmation';
import ConfirmationDialog from 'Components/Dialogs/Confirmation';
import Pagination from 'Components/Pagination/TablePagination';
import Withdrawals from '../../../../fixtures/withdrawals';

import { processWithdrawal } from 'Actions/WithrawalsActions';
import './withdrawals.css';

class WithdrawalsListComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      allSelected: false,
      showAlert: false,
      confirmation: {
        open: false,
        title: 'Approve Confirmation',
        message: '',
        selectedData: null,
      },
      alertMessage: '',
      selectedItems: 0,
      filter: {
        anchorEl: null,
        from: null,
        to: null
      },
      columns: {
        anchorEl: null,
        default: [
          { header: 'ID', key: 'id', show: true },
          { header: 'Token', key: 'token', show: true },
          { header: 'MT4 Account', key: 'm4t_login', show: true },
          { header: 'Name', key: 'name', show: true },
          { header: 'CCY', key: 'ccy', show: true },
          { header: 'MT4 Amount', key: 'mtr_amount', show: true },
          { header: 'Fee', key: 'fee', show: true },
          { header: 'Send Method', key: 'send_amount', show: true },
          { header: 'W/D Method', key: 'withdrawal_method', show: true },
          { header: 'MT4?', key: 'mt4', show: true },
          { header: 'MT4 Date', key: 'mt4_date', show: true },
          { header: 'Status', key: 'widthrawal_status', show: true },
          { header: 'Risk', key: 'risk', show: true }
        ],
        others: [
          { header: 'Email', show: false },
          { header: 'Fullname', show: false },
          { header: 'BSB', show: false },
          { header: 'SWIFT', show: false },
          { header: 'Branch', show: false },
          { header: 'Acc no/ IBAN', show: false },
          { header: 'Additional Info', show: false },
          { header: 'Fasapay ID', show: false },
          { header: 'Neteller', show: false },
          { header: 'Skrill email', show: false },
          { header: 'User Country', show: false }
        ]
      },
      withdrawalItems: [],
      suggestions: {
        id: [],
        token: [],
        status: [],
        method: []
      }
    };
  }

  componentDidMount() {
    const withdrawalItems = Withdrawals;
    const suggestions = {
      id: withdrawalItems.map(withdrawalItem => ({
        value: withdrawalItem.id,
        label: withdrawalItem.id
      })),
      token: withdrawalItems.map(withdrawalItem => ({
        value: withdrawalItem.token,
        label: withdrawalItem.token
      })),
      status: _.uniqBy(
        withdrawalItems.map(withdrawalItem => ({
          value: withdrawalItem.withdrawal_status,
          label: withdrawalItem.withdrawal_status
        })),
        'value'
      ),
      method: _.uniqBy(
        withdrawalItems.map(withdrawalItem => ({
          value: withdrawalItem.withdrawal_method,
          label: withdrawalItem.withdrawal_method
        })),
        'value'
      )
    };

    this.setState(state => ({
      ...state,
      withdrawalItems,
      suggestions
    }));
  }

  onSelectAllItems() {
    const { selectedItems, withdrawalItems } = this.state;
    const selectAll = selectedItems < withdrawalItems.length;

    if (selectAll) {
      const selectAllItems = withdrawalItems.map(item => {
        item.checked = true;
        return item;
      });
      this.setState({
        withdrawalItems: selectAllItems,
        selectedItems: selectAllItems.length,
        allSelected: true
      });
    } else {
      const unselectedItems = withdrawalItems.map(item => {
        item.checked = false;
        return item;
      });
      this.setState({
        selectedItems: 0,
        withdrawalItems: unselectedItems,
        allSelected: false
      });
    }
  }

  /**
   * On Select User
   */
  onSelectWithdrawal(withdrawalItem) {
    this.setState(state => {
      withdrawalItem.checked = !withdrawalItem.checked;
      let selectedItems = 0;
      const withdrawalItems = state.withdrawalItems.map(itemData => {
        if (itemData.checked) {
          selectedItems += 1;
        }
        if (itemData.id === withdrawalItem.id) {
          if (itemData.checked) {
            selectedItems += 1;
          }
          return withdrawalItem;
        }
        return itemData;
      });

      const allSelected = state.withdrawalItems.length;
      return {
        withdrawalItems,
        selectedItems,
        allSelected
      };
    });
  }

  onOpenPopover = (event, popover) => {
    const { currentTarget } = event;
    if (popover === 'filter') {
      this.setState(state => ({
        ...state,
        filter: {
          ...state.filter,
          anchorEl: currentTarget
        }
      }));
    } else {
      this.setState(state => ({
        ...state,
        columns: {
          ...state.columns,
          anchorEl: currentTarget
        }
      }));
    }
  };

  onClosePopover = popover => {
    if (popover === 'filter') {
      this.setState(state => ({
        ...state,
        filter: {
          ...state.filter,
          anchorEl: null
        }
      }));
    } else {
      this.setState(state => ({
        ...state,
        columns: {
          ...state.columns,
          anchorEl: null
        }
      }));
    }
  };

  onDateChange = (date, key) => {
    this.setState(state => {
      const { filter } = state;
      filter[key] = date;
      return {
        ...state,
        filter
      };
    });
  };

  processWithdrawal = () => {
    this.props.processWithdrawal(() => {
      this.setState(state => ({
        ...state,
        showAlert: true,
        alertMessage: 'Withdrawal batch successfully submitted for processing',
      }));
    });
  }

  approveWithdrawal() {
    this.setState(state => {
      const { withdrawalItems, confirmation } = state;
      const selectedItem = _.find(withdrawalItems, { id: confirmation.selectedData })
      selectedItem.approved = true;
      return {
        ...state,
        withdrawalItems,
        confirmation: {
          ...confirmation,
          open: false,
        }
      };
    });
  }

  checkRisks() {
    let withRisk = false;
    for(const item of this.state.withdrawalItems) {
      if (item.checked && (item.risk.length > 0 && !item.approved)) {
        withRisk = true;
        break;
      }
    }
    return withRisk;
  }

  toggleAlert() {
    this.setState(state => ({
      ...state,
      showAlert: !state.showAlert
    }))
  }

  onCloseConfirmation() {
    this.setState(state => {
      const { confirmation } = state;
      return {
        ...state,
        confirmation: {
          ...confirmation,
          open: false,
        }
      }
    });
  }

  onOpenConfirmation(selectedData) {
    this.setState(state => ({
      ...state,
      confirmation: {
        open: true,
        title: 'Approve Confirmation',
        message: 'Are you sure you want to approve this withdrawal?',
        selectedData
      }
    }));
  }

  render() {
    const {
      alertMessage,
      withdrawalItems,
      loading,
      allSelected,
      selectedItems,
      filter,
      columns,
      suggestions,
      showAlert,
      confirmation
    } = this.state;
    const { anchorEl: filtersAnchor } = filter;
    const { anchorEl: columnsAnchor } = columns;
    const openFilter = Boolean(filtersAnchor);
    const openColumns = Boolean(columnsAnchor);
    return (
      <div className="user-management">
        <Helmet>
          <title>Reactify | Finance &gt; Withdrawals</title>
          <meta name="description" content="Reactify Widgets" />
        </Helmet>
        <ConfirmationDialog
          title={confirmation.title}
          open={confirmation.open}
          message={confirmation.message}
          onClose={this.onCloseConfirmation.bind(this)}
          onYes={this.approveWithdrawal.bind(this)}
          maxWidth="sm"
        />
        <PageTitleBar
          title={<IntlMessages id="sidebar.withdrawals" />}
          match={this.props.match}
        />
        <Alert color="success" isOpen={showAlert} toggle={() => this.toggleAlert()}>
          {alertMessage}
        </Alert>
        <RctCollapsibleCard fullBlock>
          <div className="table-responsive">
            <div className="d-flex justify-content-between py-10 px-10">
              <div>
                <WitdhrawalConfirmation
                  disabled={selectedItems <= 0}
                  title="Process Withdrawals"
                  onYes={this.processWithdrawal}
                  withRisk={this.checkRisks()}
                />
              </div>
              <div>
                <Tooltip id="tooltip-icon" title="Hilde/Show Columns">
                  <IconButton
                    color="default"
                    aria-label="Hilde/Show Columns"
                    onClick={event => this.onOpenPopover(event, 'columns')}
                  >
                    <i className="material-icons">view_column</i>
                  </IconButton>
                </Tooltip>
                <Popover
                  open={openColumns}
                  anchorEl={columnsAnchor}
                  onClose={() => this.onClosePopover('columns')}
                  anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left'
                  }}
                  transformOrigin={{
                    vertical: 'top',
                    horizontal: 'center'
                  }}
                >
                  <div className="withdrawals-filters">
                    <Scrollbars
                      className="rct-scroll"
                      autoHeight
                      autoHide
                      autoHeightMin={200}
                      autoHeightMax={300}
                    >
                      <div className="row">
                        <div className="col-lg-6">
                          <h4>Default</h4>
                          <FormGroup>
                            {columns.default.map(column => (
                              <FormControlLabel
                                key={column.key}
                                control={
                                  <Checkbox
                                    color="primary"
                                    checked={column.show}
                                  />
                                }
                                label={column.header}
                              />
                            ))}
                          </FormGroup>
                        </div>
                        <div className="col-lg-6">
                          <h4>Hidden</h4>
                          <FormGroup>
                            {columns.others.map((column, key) => (
                              <FormControlLabel
                                key={key}
                                control={
                                  <Checkbox
                                    color="primary"
                                    checked={column.show}
                                  />
                                }
                                label={column.header}
                              />
                            ))}
                          </FormGroup>
                        </div>
                      </div>
                    </Scrollbars>
                  </div>
                </Popover>
                <Tooltip id="tooltip-icon" title="Filter">
                  <IconButton
                    color="default"
                    aria-label="Filter Table"
                    onClick={event => this.onOpenPopover(event, 'filter')}
                  >
                    <i className="material-icons">filter_list</i>
                  </IconButton>
                </Tooltip>
                <Popover
                  open={openFilter}
                  anchorEl={filtersAnchor}
                  onClose={() => this.onClosePopover('filter')}
                  anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left'
                  }}
                  transformOrigin={{
                    vertical: 'top',
                    horizontal: 'center'
                  }}
                >
                  <div className="withdrawals-filters">
                    <h4>Filters: </h4>
                    <div className="row">
                      <div className="col-lg-6">
                        <FormGroup className="mt-15">
                          <FormControl fullWidth>
                            <InputLabel htmlFor="withrawalsId">ID</InputLabel>
                            <Input id="withrawalsId" label="ID" placeholder="ID" />
                          </FormControl>
                        </FormGroup>
                        <FormGroup className="mt-15">
                          <AutoComplete
                            multiple
                            suggestions={suggestions.status}
                            label="Withdrawal Status"
                            placeholder="Withdrawal Status"
                          />
                        </FormGroup>
                        <FormGroup className="mt-5">
                          <DatePicker
                            fullWidth
                            clearable
                            label="Date From:"
                            onChange={date => this.onDateChange(date, 'from')}
                            value={filter.from}
                            leftArrowIcon={
                              <i className="zmdi zmdi-arrow-back" />
                            }
                            rightArrowIcon={
                              <i className="zmdi zmdi-arrow-forward" />
                            }
                          />
                        </FormGroup>
                      </div>
                      <div className="col-lg-6">
                        <FormGroup className="mt-15">
                          <FormControl fullWidth>
                            <InputLabel htmlFor="withrawalsToken">Token</InputLabel>
                            <Input id="withrawalsToken" label="Token" placeholder="Token" />
                          </FormControl>
                        </FormGroup>
                        <FormGroup className="mt-15">
                          <AutoComplete
                            multiple
                            suggestions={suggestions.method}
                            label="Withdrawal Method"
                            placeholder="Withdrawal Method"
                          />
                        </FormGroup>
                        <FormGroup className="mt-5">
                          <DatePicker
                            fullWidth
                            clearable
                            label="Date To:"
                            onChange={date => this.onDateChange(date, 'to')}
                            value={filter.to}
                            leftArrowIcon={
                              <i className="zmdi zmdi-arrow-back" />
                            }
                            rightArrowIcon={
                              <i className="zmdi zmdi-arrow-forward" />
                            }
                          />
                        </FormGroup>
                      </div>
                      <div className="col-lg-6 align-items-xs-center mt-20">
                        <Button variant="flat">Clear</Button>
                      </div>
                      <div className="col-lg-6 align-items-xs-center mt-20">
                        <Button variant="flat" className="text-primary">
                          Apply Filters
                        </Button>
                      </div>
                    </div>
                  </div>
                </Popover>
              </div>
            </div>
            <Table className="phoenix-table table table-middle table-hover mb-0">
              <TableHead>
                <TableRow>
                  <TableCell>
                    <FormControlLabel
                      control={
                        <Checkbox
                          indeterminate={
                            selectedItems > 0 &&
                            selectedItems < withdrawalItems.length
                          }
                          checked={allSelected}
                          onChange={e => this.onSelectAllItems(e)}
                          value="all"
                          color="primary"
                        />
                      }
                    />
                  </TableCell>
                  {columns.default.map(col => {
                    if (col.show) {
                      return <TableCell key={col.key}>{col.header}</TableCell>;
                    }
                    return null;
                  })}
                  {columns.others.map(col => {
                    if (col.show) {
                      return <TableCell>{col.header}</TableCell>;
                    }
                    return null;
                  })}
                  <TableCell>Action</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {withdrawalItems &&
                  withdrawalItems.map((withdrawalItem, key) => (
                    <TableRow key={key} 
                      className={
                        (withdrawalItem.risk.length > 0 && !withdrawalItem.approved) ? 
                        'withrawals-risk-row' : null}
                      >
                      <TableCell>
                        <FormControlLabel
                          control={
                            <Checkbox
                              checked={withdrawalItem.checked || false}
                              onChange={() =>
                                this.onSelectWithdrawal(withdrawalItem)
                              }
                              color="primary"
                            />
                          }
                        />
                      </TableCell>
                      <TableCell>{withdrawalItem.id}</TableCell>
                      <TableCell>{withdrawalItem.token}</TableCell>
                      <TableCell>{withdrawalItem.m4t_login}</TableCell>
                      <TableCell>{withdrawalItem.name}</TableCell>
                      <TableCell>{withdrawalItem.ccy}</TableCell>
                      <TableCell>{withdrawalItem.mt4_amount}</TableCell>
                      <TableCell>{withdrawalItem.fee}</TableCell>
                      <TableCell>{withdrawalItem.send_amount}</TableCell>
                      <TableCell>{withdrawalItem.withdrawal_method}</TableCell>
                      <TableCell>{withdrawalItem.mt4}</TableCell>
                      <TableCell>{withdrawalItem.mt4_date}</TableCell>
                      <TableCell>{withdrawalItem.withdrawal_status}</TableCell>
                      <TableCell>
                        <span
                          className={`badge badge-pill badge-${
                            withdrawalItem.approved ? 'warning' : 'danger'
                          }`}
                        >
                          {withdrawalItem.risk}
                        </span>
                      </TableCell>
                      <TableCell className="list-action">
                        <Tooltip title="View">
                          <IconButton className="text-primary">
                            <i className="zmdi zmdi-eye" />
                          </IconButton>
                        </Tooltip>
                        <Tooltip title="Edit">
                          <IconButton className="text-warning">
                            <i className="zmdi zmdi-edit" />
                          </IconButton>
                        </Tooltip>
                        <Tooltip title="Approve">
                          <IconButton
                            className="text-success"
                            style={
                              withdrawalItem.risk.length === 0 ||
                              withdrawalItem.approved
                                ? { display: 'none' }
                                : null
                            }
                            onClick={() => this.onOpenConfirmation(withdrawalItem.id)}
                          >
                            <i
                              className="zmdi zmdi-check"
                            />
                          </IconButton>
                        </Tooltip>
                      </TableCell>
                    </TableRow>
                  ))}
              </TableBody>
              <TableFooter>
                <TableRow>
                  <TableCell colSpan="100%">
                    <Pagination />
                  </TableCell>
                </TableRow>
              </TableFooter>
            </Table>
          </div>
          {loading && <RctSectionLoader />}
        </RctCollapsibleCard>
      </div>
    );
  }
}

export default connect(null, { processWithdrawal })(WithdrawalsListComponent)
