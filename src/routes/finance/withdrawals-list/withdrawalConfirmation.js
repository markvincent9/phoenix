import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

class Confirmation extends Component {
  state = {
    open: false
  };

  handleClickOpen() {
    this.setState({ open: true });
  }

  handleClose(answer) {
    if (answer === 'yes') {
      this.props.onYes();
    }
    this.setState({ open: false });
  }

  render() {
    const { disabled, withRisk, title } = this.props;
    return (
      <div>
        <Button
          disabled={disabled}
          variant="raised"
          onClick={() => this.handleClickOpen()}
          color="primary"
          className="text-white"
          style={{ marginTop: '7px' }}
        >
          {title}
        </Button>
        <Dialog
          open={this.state.open}
          onClose={() => this.handleClose()}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">
            Process Withdrawals
            {withRisk ? (
              <span className="badge badge-danger badge-pill ml-10">
                Risks Found
              </span>
            ) : null}
          </DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              {withRisk
                ? 'Some withdrawals with risk has been selected, please approve or untick them to proceed.'
                : 'Are you sure you want to proceed with this batch?'}
            </DialogContentText>
          </DialogContent>
          <DialogActions hidden={withRisk}>
            <Button
              variant="raised"
              onClick={() => this.handleClose('no')}
              className="btn-primary text-white"
              autoFocus
            >
              No
            </Button>
            <Button
              variant="raised"
              onClick={() => this.handleClose('yes')}
              className="btn-success text-white"
            >
              Yes
            </Button>
          </DialogActions>
          <DialogActions hidden={!withRisk}>
            <Button
              variant="raised"
              onClick={() => this.handleClose()}
              className="btn-primary text-white"
            >
              OK
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

export default Confirmation;
