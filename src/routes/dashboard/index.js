/**
 * Dasboard Routes
 */
import React from "react";
import { Redirect, Route, Switch } from "react-router-dom";

// async components
import { AsyncEcommerceDashboardComponent } from "Components/AsyncComponent/AsyncComponent";

const Dashboard = ({ match }) => (
  <div className="dashboard-wrapper">
    <Route
      path={`${match.url}/ecommerce`}
      component={AsyncEcommerceDashboardComponent}
    />
  </div>
);

export default Dashboard;
