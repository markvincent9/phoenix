import React from 'react';
import { connect } from 'react-redux';
import { Redirect, Route, Switch } from 'react-router-dom';
import { NotificationContainer } from 'react-notifications';

// rct theme provider
import {
  AsyncSessionLoginComponent,
  AsyncSessionRegisterComponent,
  AsyncSessionLockScreenComponent,
  AsyncSessionForgotPasswordComponent,
  AsyncSessionPage404Component,
  AsyncSessionPage500Component,
  AsyncTermsConditionComponent
} from 'Components/AsyncComponent/AsyncComponent';
import RctThemeProvider from './RctThemeProvider';

// Main App
import RctDefaultLayout from './DefaultLayout';

// app signin
import AppSignIn from './Signin';
import AppSignUp from './SignupFirebase';

/**
 * Initial Path To Check Whether User Is Logged In Or Not
 */
const InitialPath = ({ component: PassedComponent, authUser, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      authUser ? (
        <PassedComponent {...props} authUser={authUser} />
      ) : (
        <Redirect
          to={{
            pathname: '/signin',
            state: { from: props.location }
          }}
        />
      )
    }
  />
);

const App = props => {
  const { location, match, user } = props;
  if (location.pathname === '/') {
    if (user === null) {
      return <Redirect to="/signin" />;
    }
    return <Redirect to="/app/dashboard/ecommerce" />;
  }
  return (
    <RctThemeProvider>
      <NotificationContainer />
      <Switch>
        <InitialPath
          path={`${match.url}app`}
          authUser={user}
          component={RctDefaultLayout}
        />
        <Route path="/signin" component={AppSignIn} />
        <Route path="/signup" component={AppSignUp} />
        <Route path="/session/login" component={AsyncSessionLoginComponent} />
        <Route
          path="/session/register"
          component={AsyncSessionRegisterComponent}
        />
        <Route
          path="/session/lock-screen"
          component={AsyncSessionLockScreenComponent}
        />
        <Route
          path="/session/forgot-password"
          component={AsyncSessionForgotPasswordComponent}
        />
        <Route path="/session/404" component={AsyncSessionPage404Component} />
        <Route path="/session/500" component={AsyncSessionPage500Component} />
        <Route
          path="/terms-condition"
          component={AsyncTermsConditionComponent}
        />
        <Redirect to="/session/404" />
      </Switch>
    </RctThemeProvider>
  );
};

// map state to props
const mapStateToProps = ({ authUser }) => {
  const { user } = authUser;
  return { user };
};

export default connect(mapStateToProps)(App);
