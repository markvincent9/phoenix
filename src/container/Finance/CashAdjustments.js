import React, { Component } from 'react';
import { Helmet } from 'react-helmet';

import IntlMessages from 'Util/IntlMessages';
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
import CashAdjustmentsTable from 'Components/Finance/CashAdjustments/Table';
import cashAdjustmentsList from '../../../fixtures/cashAdjustments';
import 'Components/Finance/CashAdjustments/cashAdjustments.css';

export default class CashAdjustments extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      columns: [
        { name: 'ID', key: 'id', show: true },
        { name: 'User', key: 'user', show: true },
        { name: 'MT4 Account', key: 'account', show: true },
        { name: 'Deposits', key: 'deposits', show: true },
        { name: 'Reference', key: 'reference', show: true },
        { name: 'Amount', key: 'amount', show: true },
        { name: 'Amound (AUD)', key: 'amoundAud', show: true },
        { name: 'Comment', key: 'comment', show: true },
        { name: 'Amount (GBP)', key: 'amounGbp', show: true },
        { name: 'Department', key: 'department', show: true },
        { name: 'Request By', key: 'requestedBy', show: true },
        { name: 'Created Date', key: 'created', show: true },
        { name: 'Update Date', key: 'update', show: true },
        { name: 'Rate (AUD)', key: 'rateAud', show: true },
        { name: 'Rate (GBP)', key: 'rateGbp', show: true },
        { name: 'Bank Reference', key: 'bankReference', show: true }
      ]
    };
  }

  render() {
    const { loading, columns } = this.state;
    const tableOptions = {
      columns,
      data: cashAdjustmentsList
    };

    return (
      <div>
        <Helmet>
          <title>Phoenix | Finance &gt; Cash Adjustments</title>
        </Helmet>
        <PageTitleBar
          title={<IntlMessages id="sidebar.cashAdjustments" />}
          match={this.props.match}
        />
        <RctCollapsibleCard fullBlock>
          <CashAdjustmentsTable {...tableOptions} />
        </RctCollapsibleCard>
        {loading && <RctSectionLoader />}
      </div>
    );
  }
}
