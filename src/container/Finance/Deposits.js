import React, { Component } from 'react';
import { Helmet } from 'react-helmet';

import IntlMessages from 'Util/IntlMessages';
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
import DepositsTable from 'Components/Finance/Deposits/Table';
import depositsList from '../../../fixtures/deposits';

export default class Deposits extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      columns: [
        { name: 'ID', key: 'id', show: true },
        { name: 'User ID', key: 'userId', show: true },
        { name: 'MT4 Account', key: 'mt4AccountId', show: true },
        { name: 'Amount', key: 'amount', show: true },
        { name: 'MT4 Ticket', key: 'mt4Ticket', show: true },
        { name: 'Created Date', key: 'created', show: true },
        { name: 'Updated Date', key: 'updated', show: true },
        { name: 'Currency', key: 'currency', show: true },
        { name: 'Processed', key: 'isProcessed', show: true },
        {
          name: 'Additional Information',
          key: 'additionalInformation',
          show: true
        },
        { name: 'Comment', key: 'comment', show: true },
        { name: 'Gateway', key: 'gateway', show: true },
        { name: 'Type', key: 'type', show: true },
        { name: 'Conversion Rate', key: 'conversionRate', show: true },
        { name: 'Paired Transfer Id', key: 'pairedTransferId', show: true }
      ]
    };
  }

  render() {
    const { loading, columns } = this.state;
    const tableOptions = {
      columns,
      data: depositsList
    };

    return (
      <div>
        <Helmet>
          <title>Phoenix | Finance &gt; Deposits</title>
        </Helmet>
        <PageTitleBar
          title={<IntlMessages id="sidebar.deposits" />}
          match={this.props.match}
        />
        <RctCollapsibleCard fullBlock>
          <DepositsTable {...tableOptions} />
        </RctCollapsibleCard>
        {loading && <RctSectionLoader />}
      </div>
    );
  }
}
