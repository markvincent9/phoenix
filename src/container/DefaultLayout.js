/**
 * App Routes
 */
import React from 'react';

// app default layout
import RctAppLayout from 'Components/RctAppLayout';
import { Route } from 'react-router-dom';
// router service
import routerService from '../services/_routerService';

const DefaultLayout = props => {
  const { match } = props;
  return (
    <RctAppLayout>
      {routerService &&
        routerService.map(route => (
          <Route
            key={`${match.url}/${route.path}`}
            path={`${match.url}/${route.path}`}
            component={route.component}
          />
        ))}
    </RctAppLayout>
  );
};

export default DefaultLayout;
