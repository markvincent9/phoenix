/**
 * Rct Theme Provider
 */
import React, { Fragment } from 'react';
import { MuiThemeProvider } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { IntlProvider } from 'react-intl';

// App locale
import AppLocale from '../lang';

// themes
import theme from './themes/primaryTheme';

const RctThemeProvider = props => {
  const { locale, children } = props;
  const currentAppLocale = AppLocale[locale.locale];

  return (
    <MuiThemeProvider theme={theme}>
      <IntlProvider
        locale={currentAppLocale.locale}
        messages={currentAppLocale.messages}
      >
        <Fragment>{children}</Fragment>
      </IntlProvider>
    </MuiThemeProvider>
  );
};

// map state to props
const mapStateToProps = ({ settings }) => settings;

export default connect(mapStateToProps)(RctThemeProvider);
