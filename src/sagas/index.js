/**
 * Root Sagas
 */
import { all } from 'redux-saga/effects';

// sagas
import authSagas from './Auth';
import withdrawalSagas from './Withdrawals';

export default function* rootSaga() {
  yield all([authSagas(), withdrawalSagas()]);
}
