import { delay } from 'redux-saga';
import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import { PROCESS_WITHDRAWAL } from 'Actions/types';

import { block, blockSuccess, blockFail } from 'Actions/BlockActions';

function* submitWithrawals({ callback }) {
  try {
    yield put(block());
    yield call(delay, 5000);
    yield put(blockSuccess());
    yield call(callback);
  } catch (e) {
    console.error(e);
    yield put(blockFail());
  }
}

export function* processWithrawal() {
  yield takeEvery(PROCESS_WITHDRAWAL, submitWithrawals);
}

export default function* rootSaga() {
  yield all([fork(processWithrawal)]);
}
