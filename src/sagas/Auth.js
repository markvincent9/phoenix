import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import { LOGIN_USER, LOGOUT_USER } from 'Actions/types';

import {
  signinUserSuccess,
  signinUserFailure,
  logoutUserSuccess,
  logoutUserFailure
} from 'Actions';

// import API from 'Api/public';
import authConfig from 'Constants/auth';
import jwtDecode from 'jwt-decode';
import { Deserializer } from 'jsonapi-serializer';

const deserializer = new Deserializer();

// eslint-disable-next-line
const signInUserWithEmailPasswordRequest = async (email, password) => {
  // const response = await API.post('/auth/login', {
  //   data: {
  //     type: 'login',
  //     attributes: {
  //       email,
  //       password
  //     }
  //   }
  // }).then(res => res.data);

  const response = {
    data: {
      type: 'accessToken',
      attributes: {
        token:
          // eslint-disable-next-line
          'eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJyb2xlcyI6WyJBZG1pbiJdLCJlbWFpbCI6ImFkbWluQHBlcHBlcnN0b25lLmNvbSIsInVzZXJuYW1lIjoiYWRtaW4iLCJpYXQiOjE1NDE3MzU1MzcsImV4cCI6MTU0MTczNzMzNywiYXVkIjoiaHR0cDovL2xvY2FsaG9zdCIsImlzcyI6Imh0dHBzOi8vcGVwcGVyc3RvbmUuY29tIiwic3ViIjoiMDQ1MTM5YzItZjZkYi00ZjdkLWIyMzEtMjJkNTJmMzk5Yzc0IiwianRpIjoiNjhiMzI2MGYtN2M5MC00NGZiLThjOGYtYzUyMjUzNzZkNDE2In0.asxGwmx2BL_J1D8hvNuxTv3PH_iR4JrA4ujuRexMoKs9kpU2iYq7Ah8-wtUv2as3nigBckk5cDQDEb1cIY3EzQ'
      }
    }
  };

  if (!response) return null;

  const { token } = await deserializer.deserialize(response);
  return token;
};

const signOutRequest = async () => {};

/**
 * Signin User With Email & Password
 */
function* signInUserWithEmailPassword({ payload }) {
  const { email, password } = payload.user;
  const { history } = payload;
  try {
    const token = yield call(
      signInUserWithEmailPasswordRequest,
      email,
      password
    );
    if (token === null) {
      yield put(signinUserFailure('Invalid email or password'));
    } else {
      const { roles, username } = jwtDecode(token);
      const user = {
        email,
        roles,
        username
      };

      localStorage.setItem(authConfig.PHOENIX_USER, token);
      yield put(signinUserSuccess(user));
      history.push('/');
    }
  } catch (error) {
    if (error.response && error.response.status === 401) {
      yield put(signinUserFailure('Invalid email or password'));
    } else {
      yield put(signinUserFailure(error.message));
    }
  }
}

/**
 * Signout User
 */
function* signOut() {
  try {
    yield call(signOutRequest);
    localStorage.removeItem(authConfig.PHOENIX_USER);
    yield put(logoutUserSuccess());
  } catch (error) {
    yield put(logoutUserFailure());
  }
}

/**
 * Signin User In Firebase
 */
export function* signinUser() {
  yield takeEvery(LOGIN_USER, signInUserWithEmailPassword);
}

/**
 * Signout User From Firebase
 */
export function* signOutUser() {
  yield takeEvery(LOGOUT_USER, signOut);
}

export default function* rootSaga() {
  yield all([fork(signinUser), fork(signOutUser)]);
}
