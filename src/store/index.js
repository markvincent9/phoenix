/**
 * Redux Store
 */
import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import blockUiMiddleware from 'react-block-ui/reduxMiddleware';

import reducers from '../reducers';
import RootSaga from '../sagas';

const sagaMiddleware = createSagaMiddleware();

const middlewares = [blockUiMiddleware, sagaMiddleware];

export const configureStore = initialState => {
  const store = createStore(
    reducers,
    initialState,
    compose(applyMiddleware(...middlewares))
  );

  sagaMiddleware.run(RootSaga);

  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('../reducers/index', () => {
      // eslint-disable-next-line
      const nextRootReducer = require("../reducers/index");
      store.replaceReducer(nextRootReducer);
    });
  }

  return store;
};
