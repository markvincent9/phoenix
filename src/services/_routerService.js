import Users from 'Routes/users';
import Ecommerce from 'Routes/ecommerce';
import Dashboard from 'Routes/dashboard';
import Finance from 'Routes/finance';

// add more routes
export default [
  {
    path: 'dashboard',
    component: Dashboard
  },
  {
    path: 'ecommerce',
    component: Ecommerce
  },
  {
    path: 'users',
    component: Users
  },
  {
    path: 'finance',
    component: Finance
  }
];
