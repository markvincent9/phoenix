import axios from 'axios';

export default axios.create({
  baseURL: process.env.API_ENDPOINT,
  timeout: 15000,
  headers: {
    'x-api-key': process.env.API_KEY,
    'Content-Type': 'application/vnd.api+json'
  }
});
