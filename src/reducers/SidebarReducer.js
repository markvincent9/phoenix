/**
 * Sidebar Reducers
 */
import update from 'react-addons-update';
import { TOGGLE_MENU } from 'Actions/types';

// nav links
import navLinks from 'Components/Sidebar/NavLinks';

const INIT_STATE = {
  sidebarMenus: navLinks
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case TOGGLE_MENU: {
      const index = state.sidebarMenus[
        action.payload.stateCategory
      ].subMenus.indexOf(action.payload.menu);
      Object.keys(state.sidebarMenus).forEach(key => {
        const obj = state.sidebarMenus[key].subMenus;
        for (let i = 0; i < obj.length; i += 1) {
          const element = obj[i];
          if (element.open) {
            if (key === action.payload.stateCategory) {
              return update(state, {
                sidebarMenus: {
                  [key]: {
                    subMenus: {
                      [i]: {
                        open: { $set: false }
                      },
                      [index]: {
                        open: { $set: !action.payload.menu.open }
                      }
                    }
                  }
                }
              });
            }
            return update(state, {
              sidebarMenus: {
                [key]: {
                  subMenus: {
                    [i]: {
                      open: { $set: false }
                    }
                  }
                },
                [action.payload.stateCategory]: {
                  subMenus: {
                    [index]: {
                      open: { $set: !action.payload.menu.open }
                    }
                  }
                }
              }
            });
          }
        }
      });
      return update(state, {
        sidebarMenus: {
          [action.payload.stateCategory]: {
            subMenus: {
              [index]: {
                open: { $set: !action.payload.menu.open }
              }
            }
          }
        }
      });
    }
    default:
      return { ...state };
  }
};
