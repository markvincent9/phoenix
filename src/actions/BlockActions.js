import { BLOCK, BLOCK_SUCCESS, BLOCK_FAILURE } from 'Actions/types';

export const block = () => ({
  type: BLOCK
});

export const blockSuccess = () => ({
  type: BLOCK_SUCCESS
});

export const blockFail = () => ({
  type: BLOCK_FAILURE
});
