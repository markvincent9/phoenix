import { PROCESS_WITHDRAWAL } from 'Actions/types';

export const processWithdrawal = callback => ({
  type: PROCESS_WITHDRAWAL,
  callback
});
