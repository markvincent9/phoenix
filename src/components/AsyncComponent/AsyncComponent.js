/**
 * AsyncComponent
 * Code Splitting Component / Server Side Rendering
 */
import React from 'react';
import Loadable from 'react-loadable';

// rct page loader
import RctPageLoader from 'Components/RctPageLoader/RctPageLoader';

// ecommerce dashboard
const AsyncEcommerceDashboardComponent = Loadable({
  loader: () => import('Routes/dashboard/ecommerce'),
  loading: () => <RctPageLoader />
});

// shop list
const AsyncShoplistComponent = Loadable({
  loader: () => import('Routes/ecommerce/shop-list'),
  loading: () => <RctPageLoader />
});

// shop grid
const AsyncShopGridComponent = Loadable({
  loader: () => import('Routes/ecommerce/shop-grid'),
  loading: () => <RctPageLoader />
});

// shop
const AsyncShopComponent = Loadable({
  loader: () => import('Routes/ecommerce/shop'),
  loading: () => <RctPageLoader />
});

// cart
const AsyncCartComponent = Loadable({
  loader: () => import('Routes/ecommerce/cart'),
  loading: () => <RctPageLoader />
});

// checkout
const AsyncCheckoutComponent = Loadable({
  loader: () => import('Routes/ecommerce/checkout'),
  loading: () => <RctPageLoader />
});

// invoice
const AsyncInvoiceComponent = Loadable({
  loader: () => import('Routes/ecommerce/invoice'),
  loading: () => <RctPageLoader />
});

/* ---------------- Users ------------------*/

// Users List
const AsyncUsersListComponent = Loadable({
  loader: () => import('Routes/users/user-list'),
  loading: () => <RctPageLoader />
});

// Users Profile
const AsyncUserProfileComponent = Loadable({
  loader: () => import('Routes/users/user-profile'),
  loading: () => <RctPageLoader />
});

// Users Profile 1
const AsyncUserProfile1Component = Loadable({
  loader: () => import('Routes/users/user-profile-1'),
  loading: () => <RctPageLoader />
});

// Users Management
const AsyncUserManagementComponent = Loadable({
  loader: () => import('Routes/users/user-management'),
  loading: () => <RctPageLoader />
});

/* ---------------- Finance ------------------*/

// Finance Withdrawal List Management
const AsyncFinanceWithdrawalsListComponent = Loadable({
  loader: () => import('Routes/finance/withdrawals-list'),
  loading: () => <RctPageLoader />
});

const AsyncFinanceDepositsListComponent = Loadable({
  loader: () => import('Containers/Finance/Deposits.js'),
  loading: () => <RctPageLoader />
});

const AsyncFinanceCashAdjustmentsComponent = Loadable({
  loader: () => import('Containers/Finance/CashAdjustments'),
  loading: () => <RctPageLoader />
});

/* ---------------- Session ------------------*/

// Session Login
const AsyncSessionLoginComponent = Loadable({
  loader: () => import('Routes/session/login'),
  loading: () => <RctPageLoader />
});

// Session Register
const AsyncSessionRegisterComponent = Loadable({
  loader: () => import('Routes/session/register'),
  loading: () => <RctPageLoader />
});

// Session Lock Screen
const AsyncSessionLockScreenComponent = Loadable({
  loader: () => import('Routes/session/lock-screen'),
  loading: () => <RctPageLoader />
});

// Session Forgot Password
const AsyncSessionForgotPasswordComponent = Loadable({
  loader: () => import('Routes/session/forgot-password'),
  loading: () => <RctPageLoader />
});

// Session Page 404
const AsyncSessionPage404Component = Loadable({
  loader: () => import('Routes/session/404'),
  loading: () => <RctPageLoader />
});

// Session Page 500
const AsyncSessionPage500Component = Loadable({
  loader: () => import('Routes/session/500'),
  loading: () => <RctPageLoader />
});

export {
  AsyncUsersListComponent,
  AsyncUserProfileComponent,
  AsyncUserProfile1Component,
  AsyncUserManagementComponent,
  AsyncSessionLoginComponent,
  AsyncSessionRegisterComponent,
  AsyncSessionLockScreenComponent,
  AsyncSessionForgotPasswordComponent,
  AsyncSessionPage404Component,
  AsyncSessionPage500Component,
  AsyncEcommerceDashboardComponent,
  AsyncShoplistComponent,
  AsyncShopGridComponent,
  AsyncInvoiceComponent,
  AsyncShopComponent,
  AsyncCartComponent,
  AsyncCheckoutComponent,
  AsyncFinanceWithdrawalsListComponent,
  AsyncFinanceDepositsListComponent,
  AsyncFinanceCashAdjustmentsComponent
};
