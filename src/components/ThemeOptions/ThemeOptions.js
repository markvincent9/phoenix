/**
 * Theme Options
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { DropdownToggle, DropdownMenu, Dropdown } from 'reactstrap';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { Scrollbars } from 'react-custom-scrollbars';
import Switch from '@material-ui/core/Switch';
import Tooltip from '@material-ui/core/Tooltip';
import $ from 'jquery';
import AppConfig from 'Constants/AppConfig';

// redux actions
import { miniSidebarAction } from 'Actions';

// intl messages
import IntlMessages from 'Util/IntlMessages';

class ThemeOptions extends Component {
  state = {
    themeOptionPanelOpen: false
  };

  componentDidMount() {
    const { miniSidebar } = this.props;
    if (miniSidebar) {
      this.miniSidebarHanlder(true);
    }
  }

  /**
   * Function To Toggle Theme Option Panel
   */
  toggleThemePanel() {
    this.setState(previousState => ({
      themeOptionPanelOpen: !previousState.themeOptionPanelOpen
    }));
  }

  /**
   * Mini Sidebar Event Handler
   */
  miniSidebarHanlder(isTrue) {
    if (isTrue) {
      $('body').addClass('mini-sidebar');
    } else {
      $('body').removeClass('mini-sidebar');
    }
    setTimeout(() => {
      this.props.miniSidebarAction(isTrue);
    }, 100);
  }

  render() {
    const { miniSidebar, navCollapsed } = this.props;
    return (
      <div className="fixed-plugin">
        {AppConfig.enableThemeOptions && (
          <Dropdown
            isOpen={this.state.themeOptionPanelOpen}
            toggle={() => this.toggleThemePanel()}
          >
            <DropdownToggle className="bg-primary">
              <Tooltip title="Theme Options" placement="left">
                <i className="zmdi zmdi-settings font-2x tour-step-6 spin-icon" />
              </Tooltip>
            </DropdownToggle>
            <DropdownMenu>
              <Scrollbars
                className="rct-scroll"
                autoHeight
                autoHeightMin={100}
                autoHeightMax={530}
                autoHide
                autoHideDuration={100}
              >
                <ul className="list-unstyled mb-0 p-10 app-settings">
                  <li className="header-title mb-10">
                    <IntlMessages id="themeOptions.appSettings" />
                  </li>
                  <li className="header-title mini-sidebar-option">
                    <FormControlLabel
                      control={
                        <Switch
                          disabled={navCollapsed}
                          checked={miniSidebar}
                          onChange={e =>
                            this.miniSidebarHanlder(e.target.checked)
                          }
                          className="switch-btn"
                        />
                      }
                      label={<IntlMessages id="themeOptions.miniSidebar" />}
                      className="m-0"
                    />
                  </li>
                </ul>
              </Scrollbars>
            </DropdownMenu>
          </Dropdown>
        )}
      </div>
    );
  }
}

// map state to props
const mapStateToProps = ({ settings }) => settings;

export default connect(
  mapStateToProps,
  {
    miniSidebarAction
  }
)(ThemeOptions);
