import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContentText from '@material-ui/core/DialogContentText';

export default props => {
  const { title, message, onYes, onClose, ...other } = props;
  return (
    <Dialog
      {...other}
      disableBackdropClick
      disableEscapeKeyDown
      aria-labelledby="confirmation-dialog-title"
    >
      <DialogTitle id="confirmation-dialog-title">{title}</DialogTitle>
      <DialogContent>
        <DialogContentText>{message}</DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button
          variant="raised"
          size="small"
          onClick={onClose}
          className="btn-primary text-white"
        >
          No
        </Button>
        <Button
          variant="raised"
          size="small"
          onClick={() => onYes()}
          className="btn-success text-white"
        >
          Yes
        </Button>
      </DialogActions>
    </Dialog>
  );
};
