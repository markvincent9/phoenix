/**
 * Sidebar Content
 */
import React, { Component } from 'react';
import List from '@material-ui/core/List';
import ListSubheader from '@material-ui/core/ListSubheader';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import _ from 'lodash';
import { onToggleMenu } from 'Actions';
import { isAllowed } from 'Components/Users';
import NavMenuItem from './NavMenuItem';

class SidebarContent extends Component {
  toggleMenu(menu, stateCategory) {
    const data = {
      menu,
      stateCategory
    };
    this.props.onToggleMenu(data);
  }

  showMenu(subMenus) {
    const childRoutes = _.flatten(_.map(subMenus, 'child_routes'));
    const allowedRoles = _.reduce(
      childRoutes,
      (arr, childRoute) => {
        if (childRoute.allowedRoles)
          arr = _.union(arr, childRoute.allowedRoles);
        return arr;
      },
      []
    );

    return isAllowed(this.props.user, allowedRoles);
  }

  renderSidebar() {
    const {
      sidebar: { sidebarMenus },
      user
    } = this.props;
    return Object.keys(sidebarMenus).map(category => {
      const sidebarMenu = sidebarMenus[category];
      const showMenu = this.showMenu(sidebarMenu.subMenus);
      if (showMenu) {
        return (
          <List
            key={`sidebarMenu_${category}`}
            className="rct-mainMenu p-0 m-0 list-unstyled"
            subheader={
              <ListSubheader className="side-title" component="li">
                {sidebarMenu.title}
              </ListSubheader>
            }
          >
            {sidebarMenu.subMenus.map((menu, key) => (
              <NavMenuItem
                user={user}
                menu={menu}
                key={key}
                onToggleMenu={() => this.toggleMenu(menu, category)}
              />
            ))}
          </List>
        );
      }
      return null;
    });
  }

  render() {
    return (
      <div className="rct-sidebar-nav">
        <nav className="navigation">{this.renderSidebar()}</nav>
      </div>
    );
  }
}

// map state to props
const mapStateToProps = ({ sidebar, authUser }) => ({
  sidebar,
  user: authUser.user
});

export default withRouter(
  connect(
    mapStateToProps,
    {
      onToggleMenu
    }
  )(SidebarContent)
);
