import { ADMIN, USER } from 'Constants/roles';
// sidebar nav links
export default {
  dashboards: {
    title: 'General',
    subMenus: [
      {
        menu_title: 'Dashboard',
        menu_icon: 'zmdi zmdi-view-dashboard',
        child_routes: [
          {
            menu_title: 'Dashboard',
            path: '/app/dashboard/ecommerce',
            allowedRoles: [ADMIN, USER]
          }
        ]
      },
      {
        menu_title: 'Ecommerce',
        menu_icon: 'zmdi zmdi-shopping-cart',
        child_routes: [
          {
            path: '/app/ecommerce/shop',
            menu_title: 'Shop',
            allowedRoles: [ADMIN, USER]
          },
          {
            path: '/app/ecommerce/cart',
            menu_title: 'Cart',
            allowedRoles: [ADMIN, USER]
          },
          {
            path: '/app/ecommerce/checkout',
            menu_title: 'Checkout',
            allowedRoles: [ADMIN, USER]
          },
          {
            path: '/app/ecommerce/shop-list',
            menu_title: 'Shop List',
            allowedRoles: [ADMIN, USER]
          },
          {
            path: '/app/ecommerce/shop-grid',
            menu_title: 'Shop Grid',
            allowedRoles: [ADMIN, USER]
          },
          {
            path: '/app/ecommerce/invoice',
            menu_title: 'Invoice',
            allowedRoles: [ADMIN, USER]
          }
        ]
      }
    ]
  },
  users: {
    title: 'Applications',
    subMenus: [
      {
        menu_title: 'Users',
        menu_icon: 'zmdi zmdi-accounts',
        child_routes: [
          {
            path: '/app/users/user-profile-1',
            menu_title: 'User Profile 1',
            allowedRoles: [ADMIN, USER]
          },
          {
            path: '/app/users/user-profile',
            menu_title: 'User Profile 2',
            allowedRoles: [ADMIN, USER]
          },
          {
            path: '/app/users/user-management',
            menu_title: 'User Management',
            allowedRoles: [ADMIN]
          },
          {
            path: '/app/users/user-list',
            menu_title: 'User List',
            allowedRoles: [ADMIN]
          }
        ]
      },
      {
        menu_title: 'Finance',
        menu_icon: 'zmdi zmdi-money-box',
        child_routes: [
          {
            path: '/app/finance/withdrawals',
            menu_title: 'Withdrawals',
            allowedRoles: [ADMIN]
          },
          {
            path: '/app/finance/deposits',
            menu_title: 'Deposits',
            allowedRoles: [ADMIN]
          },
          {
            path: '/app/finance/cash-adjustments',
            menu_title: 'Cash Adjustments',
            allowedRoles: [ADMIN]
          }
        ]
      }
    ]
  }
};
