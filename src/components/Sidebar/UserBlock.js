/**
 * User Block Component
 */
import React, { Component } from 'react';
import { Badge, Dropdown, DropdownToggle, DropdownMenu } from 'reactstrap';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { NotificationManager } from 'react-notifications';

// components

// redux action
import { logoutUser } from 'Actions';

// intl messages
import IntlMessages from 'Util/IntlMessages';
import SupportPage from '../Support/Support';

class UserBlock extends Component {
  state = {
    userDropdownMenu: false,
    isSupportModal: false
  };

  /**
   * On Close Support Page
   */
  onCloseSupportPage() {
    this.setState({ isSupportModal: false });
  }

  /**
   * On Submit Support Page
   */
  onSubmitSupport() {
    this.setState({ isSupportModal: false });
    NotificationManager.success('Message has been sent successfully!');
  }

  /**
   * Open Support Modal
   */
  openSupportModal() {
    this.setState({ isSupportModal: true });
  }

  /**
   * Toggle User Dropdown Menu
   */
  toggleUserDropdownMenu() {
    this.setState(previousState => ({
      userDropdownMenu: !previousState.userDropdownMenu
    }));
  }

  /**
   * Logout User
   */
  logoutUser() {
    this.props.logoutUser();
  }

  render() {
    const { username, email } = this.props;
    return (
      <div className="top-sidebar">
        <div className="sidebar-user-block">
          <Dropdown
            isOpen={this.state.userDropdownMenu}
            toggle={() => this.toggleUserDropdownMenu()}
            className="rct-dropdown"
          >
            <DropdownToggle tag="div" className="d-flex align-items-center">
              <div className="user-profile">
                <img
                  src={require('Assets/avatars/user-15.jpg')}
                  alt="user profile"
                  className="img-fluid rounded-circle"
                  width="50"
                  height="100"
                />
              </div>
              <div className="user-info">
                <span className="user-name ml-4">{username}</span>
                <i className="zmdi zmdi-chevron-down dropdown-icon mx-4" />
              </div>
            </DropdownToggle>
            <DropdownMenu>
              <ul className="list-unstyled mb-0">
                <li className="p-15 border-bottom user-profile-top bg-primary rounded-top">
                  <p className="text-white mb-0 fs-14">{username}</p>
                  <span className="text-white fs-14">{email}</span>
                </li>
                <li>
                  <Link
                    to={{
                      pathname: '/app/users/user-profile-1',
                      state: { activeTab: 0 }
                    }}
                  >
                    <i className="zmdi zmdi-account text-primary mr-3" />
                    <IntlMessages id="widgets.profile" />
                  </Link>
                </li>
                <li>
                  <Link
                    to={{
                      pathname: '/app/users/user-profile-1',
                      state: { activeTab: 2 }
                    }}
                  >
                    <i className="zmdi zmdi-comment-text-alt text-success mr-3" />
                    <IntlMessages id="widgets.messages" />
                    <Badge color="danger" className="pull-right">
                      3
                    </Badge>
                  </Link>
                </li>
                <li className="border-top">
                  {/* eslint-disable-next-line */}
                  <a
                    href="javascript:void(0)"
                    onClick={() => this.logoutUser()}
                  >
                    <i className="zmdi zmdi-power text-danger mr-3" />
                    <IntlMessages id="widgets.logOut" />
                  </a>
                </li>
              </ul>
            </DropdownMenu>
          </Dropdown>
        </div>
        <SupportPage
          isOpen={this.state.isSupportModal}
          onCloseSupportPage={() => this.onCloseSupportPage()}
          onSubmit={() => this.onSubmitSupport()}
        />
      </div>
    );
  }
}

// map state to props
const mapStateToProps = ({ authUser }) => authUser.user;

export default connect(
  mapStateToProps,
  {
    logoutUser
  }
)(UserBlock);
