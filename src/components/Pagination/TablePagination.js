import React from 'react';
import { Pagination, PaginationItem, PaginationLink } from 'reactstrap';

export default () => (
  <Pagination className="mb-0 py-10 px-10">
    <PaginationItem>
      <PaginationLink previous href="#" />
    </PaginationItem>
    <PaginationItem active>
      <PaginationLink href="javascript:void(0)">1</PaginationLink>
    </PaginationItem>
    <PaginationItem>
      <PaginationLink href="javascript:void(0)">2</PaginationLink>
    </PaginationItem>
    <PaginationItem>
      <PaginationLink href="javascript:void(0)">3</PaginationLink>
    </PaginationItem>
    <PaginationItem>
      <PaginationLink next href="javascript:void(0)" />
    </PaginationItem>
  </Pagination>
);
