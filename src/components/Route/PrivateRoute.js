import React from 'react';
import { connect } from 'react-redux';
import { Route, Redirect } from 'react-router-dom';
import { isAllowed } from 'Components/Users';

const PrivateRoute = ({
  component: RouteComponent,
  allowedRoles,
  user,
  ...rest
}) => (
  <Route
    {...rest}
    render={props =>
      isAllowed(user, allowedRoles) ? (
        <RouteComponent {...props} />
      ) : (
        <Redirect to="/session/404" />
      )
    }
  />
);

function mapStateToProps({ authUser }) {
  return { user: authUser.user };
}

export default connect(mapStateToProps)(PrivateRoute);
