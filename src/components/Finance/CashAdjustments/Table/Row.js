import React from 'react';
import { TableCell, TableRow } from '@material-ui/core';

export default props => {
  const {
    id,
    user,
    account,
    deposits,
    reference,
    amount,
    amountAud,
    comment,
    amounGbp,
    department,
    requestedBy,
    created,
    updated,
    rateAud,
    rateGbp,
    bankReference
  } = props;

  return (
    <TableRow>
      <TableCell>{id}</TableCell>
      <TableCell>{user}</TableCell>
      <TableCell>{account}</TableCell>
      <TableCell>{deposits}</TableCell>
      <TableCell>{reference}</TableCell>
      <TableCell>{amount}</TableCell>
      <TableCell>{amountAud}</TableCell>
      <TableCell>{comment}</TableCell>
      <TableCell>{amounGbp}</TableCell>
      <TableCell>{department}</TableCell>
      <TableCell>{requestedBy}</TableCell>
      <TableCell>{created}</TableCell>
      <TableCell>{updated}</TableCell>
      <TableCell>{rateAud}</TableCell>
      <TableCell>{rateGbp}</TableCell>
      <TableCell>{bankReference}</TableCell>
    </TableRow>
  );
};
