import React from 'react';
import {
  Checkbox,
  FormGroup,
  FormControlLabel,
  IconButton,
  Popover,
  Tooltip
} from '@material-ui/core';
import { Scrollbars } from 'react-custom-scrollbars';

export default class ShowColumns extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      anchorEl: null
    };
  }

  onOpenFilter(event) {
    const { currentTarget } = event;
    this.setState(state => ({
      ...state,
      anchorEl: currentTarget
    }));
  }

  onCloseFilter() {
    this.setState(state => ({
      ...state,
      anchorEl: null
    }));
  }

  render() {
    const { anchorEl } = this.state;
    const { columns } = this.props;
    const open = Boolean(anchorEl);
    return (
      <div>
        <Tooltip id="tooltip-icon" title="Show/Hide Columns">
          <IconButton
            color="default"
            aria-label="Show/Hide Columns"
            onClick={event => this.onOpenFilter(event)}
          >
            <i className="material-icons">view_column</i>
          </IconButton>
        </Tooltip>
        <Popover
          open={open}
          anchorEl={anchorEl}
          onClose={() => this.onCloseFilter()}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left'
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'center'
          }}
        >
          <div className="cash-adjustments-columns">
            <h4>Show/Hide Columns: </h4>
            <div className="row">
              <div className="col-lg-12">
                <Scrollbars
                  className="rct-scroll"
                  autoHeight
                  autoHide
                  autoHeightMin={200}
                  autoHeightMax={300}
                >
                  <div className="row">
                    <div className="col-lg-12">
                      <FormGroup>
                        {columns &&
                          columns.map(column => (
                            <FormControlLabel
                              key={column.key}
                              control={
                                <Checkbox
                                  color="primary"
                                  checked={column.show}
                                />
                              }
                              label={column.name}
                            />
                          ))}
                      </FormGroup>
                    </div>
                  </div>
                </Scrollbars>
              </div>
            </div>
          </div>
        </Popover>
      </div>
    );
  }
}
