import React from 'react';
import {
  Checkbox,
  FormControlLabel,
  TableHead,
  TableRow,
  TableCell
} from '@material-ui/core';

export default props => {
  const {
    allSelected,
    columns,
    selectable,
    onSelectAllRows,
    indeterminate
  } = props;

  return (
    <TableHead>
      <TableRow>
        {selectable ? (
          <FormControlLabel
            control={
              <Checkbox
                indeterminate={indeterminate}
                checked={allSelected}
                onChange={e => onSelectAllRows(e)}
                value="all"
                color="primary"
              />
            }
          />
        ) : null}
        {columns &&
          columns.map(col => (
            <TableCell hidden={!col.show} key={col.key}>
              {col.name}
            </TableCell>
          ))}
      </TableRow>
    </TableHead>
  );
};
