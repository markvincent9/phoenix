import React from 'react';
import {
  Table,
  TableBody,
  TableFooter,
  TableCell,
  TableRow
} from '@material-ui/core';
import Pagination from 'Components/Pagination/TablePagination';

import Filters from './Filters';
import Header from './Header';
import Row from './Row';
import ShowColumns from './ShowColumns';

export default props => {
  const { columns, data } = props;
  return (
    <div className="table-responsive">
      <div className="d-flex justify-content-between py-10 px-10">
        <div />
        <div className="d-flex">
          <ShowColumns columns={columns} />
          <Filters />
        </div>
      </div>
      <Table className="phoenix-table table table-middle table-hover mb-0">
        <Header columns={columns} />
        <TableBody>
          {data && data.map(item => <Row key={item.id} {...item} />)}
        </TableBody>
        <TableFooter>
          <TableRow>
            <TableCell colSpan="100%">
              <Pagination />
            </TableCell>
          </TableRow>
        </TableFooter>
      </Table>
    </div>
  );
};
