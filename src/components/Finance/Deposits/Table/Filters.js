import React from 'react';
import {
  Button,
  FormControl,
  FormGroup,
  IconButton,
  Input,
  InputLabel,
  Popover,
  Tooltip
} from '@material-ui/core';
import { DatePicker } from 'material-ui-pickers';
import AutoComplete from 'Components/Select2/AutoComplete';

export default class Filter extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      anchorEl: null,
      fields: {
        textboxFilter: [
          {
            filter: 'ID',
            key: 'id'
          },
          {
            filter: 'Email',
            key: 'email'
          },
          {
            filter: 'Comment',
            key: 'comment'
          },
          {
            filter: 'Mt4 login ID',
            key: 'mt4AccountId'
          }
        ],
        dropdownFilter: [
          {
            filter: 'Payment Gateway',
            key: 'gateway',
            options: ['Skrill', 'PayPal', 'Upop', 'Neteller'].map(item => ({
              label: item,
              value: item
            }))
          }
        ],
        dateRangeFilter: {
          from: {
            filter: 'From',
            key: 'created'
          },
          to: {
            filter: 'To',
            key: 'created'
          }
        }
      }
    };
  }

  onOpenFilter(event) {
    const { currentTarget } = event;
    this.setState(state => ({
      ...state,
      anchorEl: currentTarget
    }));
  }

  onCloseFilter() {
    this.setState(state => ({
      ...state,
      anchorEl: null
    }));
  }

  render() {
    const { anchorEl, fields } = this.state;
    const { textboxFilter, dateRangeFilter, dropdownFilter } = fields;
    const open = Boolean(anchorEl);
    return (
      <div>
        <Tooltip id="tooltip-icon" title="Filter">
          <IconButton
            color="default"
            aria-label="Filter Table"
            onClick={event => this.onOpenFilter(event)}
          >
            <i className="material-icons">filter_list</i>
          </IconButton>
        </Tooltip>
        <Popover
          open={open}
          anchorEl={anchorEl}
          onClose={() => this.onCloseFilter()}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'left'
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'center'
          }}
        >
          <div className="deposits-filters">
            <h4>Filters: </h4>
            <div className="row">
              {textboxFilter &&
                textboxFilter.map(textbox => (
                  <div className="col-lg-6">
                    <FormGroup key={textbox.key} className="mt-10">
                      <FormControl fullWidth>
                        <InputLabel htmlFor={`filter-${textbox.key}`}>
                          {textbox.filter}
                        </InputLabel>
                        <Input
                          id={`filter-${textbox.key}`}
                          label={textbox.filter}
                          placeholder={textbox.filter}
                        />
                      </FormControl>
                    </FormGroup>
                  </div>
                ))}
              <div className="col-lg-12">
                {dropdownFilter &&
                  dropdownFilter.map(dropdown => (
                    <FormGroup className="mt-15">
                      <AutoComplete
                        multiple
                        suggestions={dropdown.options}
                        label={dropdown.filter}
                        placeholder={dropdown.filter}
                      />
                    </FormGroup>
                  ))}
              </div>
              <div className="col-lg-6">
                <FormGroup className="mt-10">
                  <DatePicker
                    fullWidth
                    clearable
                    label={dateRangeFilter.from.filter}
                    leftArrowIcon={<i className="zmdi zmdi-arrow-back" />}
                    rightArrowIcon={<i className="zmdi zmdi-arrow-forward" />}
                  />
                </FormGroup>
              </div>
              <div className="col-lg-6">
                <FormGroup className="mt-10">
                  <DatePicker
                    fullWidth
                    clearable
                    label={dateRangeFilter.to.filter}
                    leftArrowIcon={<i className="zmdi zmdi-arrow-back" />}
                    rightArrowIcon={<i className="zmdi zmdi-arrow-forward" />}
                  />
                </FormGroup>
              </div>
              <div className="col-lg-6 align-items-xs-center mt-20">
                <Button variant="flat">Clear</Button>
              </div>
              <div className="col-lg-6 align-items-xs-center mt-20">
                <Button variant="flat" className="text-primary">
                  Apply Filters
                </Button>
              </div>
            </div>
          </div>
        </Popover>
      </div>
    );
  }
}
