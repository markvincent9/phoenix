import React from 'react';
import { TableCell, TableRow } from '@material-ui/core';

export default props => {
  const {
    id,
    userId,
    mt4AccountId,
    amount,
    mt4Ticket,
    created,
    updated,
    currency,
    isProcessed,
    additionalInformation,
    comment,
    gateway,
    type,
    conversionRate,
    pairedTransferId
  } = props;

  return (
    <TableRow>
      <TableCell>{id}</TableCell>
      <TableCell>{userId}</TableCell>
      <TableCell>{mt4AccountId}</TableCell>
      <TableCell>{amount}</TableCell>
      <TableCell>{mt4Ticket}</TableCell>
      <TableCell>{created}</TableCell>
      <TableCell>{updated}</TableCell>
      <TableCell>{currency}</TableCell>
      <TableCell className="text-center">
        {isProcessed ? <i className="zmdi zmdi-check-all" /> : null}
      </TableCell>
      <TableCell>{additionalInformation}</TableCell>
      <TableCell>{comment}</TableCell>
      <TableCell>{gateway}</TableCell>
      <TableCell>{type}</TableCell>
      <TableCell>{conversionRate}</TableCell>
      <TableCell>{pairedTransferId}</TableCell>
    </TableRow>
  );
};
