import _ from 'lodash';
import jwtDecode from 'jwt-decode';

export const isAllowed = (user, allowedRoles) => {
  const { roles } = user;

  if (_.isEmpty(allowedRoles)) return true;

  return (
    roles &&
    !_.isEmpty(roles) &&
    !_.isEmpty(_.intersection(roles, allowedRoles))
  );
};

export const decodeToken = token => {
  const { email, roles, username } = jwtDecode(token);
  return { email, roles, username };
};
