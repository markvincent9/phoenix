/**
 * App Routes
 */
import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import Sidebar from 'react-sidebar';
import $ from 'jquery';
import { Scrollbars } from 'react-custom-scrollbars';
import classnames from 'classnames';
import ReduxBlockUi from 'react-block-ui/redux';
import CircularProgress from '@material-ui/core/CircularProgress';
import 'react-block-ui/style.css';

import Header from 'Components/Header/Header';
import SidebarContent from 'Components/Sidebar';
import Footer from 'Components/Footer/Footer';
import ThemeOptions from 'Components/ThemeOptions/ThemeOptions';

import PreloadHeader from 'Components/PreloadLayout/PreloadHeader';
import PreloadSidebar from 'Components/PreloadLayout/PreloadSidebar';

import AppConfig from 'Constants/AppConfig';

import { collapsedSidebarAction, startUserTour } from 'Actions';
import { BLOCK, BLOCK_SUCCESS, BLOCK_FAILURE } from 'Actions/types';
import './layout.css';

class MainApp extends Component {
  state = {
    loadingHeader: true,
    loadingSidebar: true
  };

  componentWillMount() {
    this.updateDimensions();
  }

  componentDidMount() {
    const { windowWidth } = this.state;
    window.addEventListener('resize', this.updateDimensions);
    if (AppConfig.enableUserTour && windowWidth > 600) {
      setTimeout(() => {
        this.props.startUserTour();
      }, 2000);
    }
    setTimeout(() => {
      this.setState({ loadingHeader: false, loadingSidebar: false });
    }, 114);
  }

  componentWillReceiveProps(nextProps) {
    const { windowWidth } = this.state;
    if (nextProps.location !== this.props.location) {
      if (windowWidth <= 1199) {
        this.props.collapsedSidebarAction(false);
      }
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.location.pathname !== prevProps.location.pathname) {
      window.scrollTo(0, 0);
    }
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateDimensions);
  }

  getScrollBarStyle() {
    return {
      height: 'calc(100vh - 50px)'
    };
  }

  updateDimensions = () => {
    this.setState({
      windowWidth: $(window).width(),
      // eslint-disable-next-line
      windowHeight: $(window).height()
    });
  };

  renderPage() {
    const { pathname } = this.props.location;
    const { children } = this.props;
    if (
      pathname === '/app/chat' ||
      pathname.startsWith('/app/mail') ||
      pathname === '/app/todo'
    ) {
      return <div className="rct-page-content p-0">{children}</div>;
    }
    return (
      <Scrollbars
        className="rct-scroll"
        autoHide
        autoHideDuration={100}
        style={this.getScrollBarStyle()}
      >
        <div className="rct-page-content">
          {children}
          <Footer />
        </div>
      </Scrollbars>
    );
  }

  renderHeader() {
    const { loadingHeader } = this.state;
    if (loadingHeader) {
      return <PreloadHeader />;
    }
    return <Header />;
  }

  renderSidebar() {
    const { loadingSidebar } = this.state;
    if (loadingSidebar) {
      return <PreloadSidebar />;
    }
    return <SidebarContent />;
  }

  render() {
    const { navCollapsed, rtlLayout, miniSidebar } = this.props.settings;
    const { windowWidth } = this.state;
    return (
      <ReduxBlockUi
        className="app"
        block={BLOCK}
        unblock={[BLOCK_SUCCESS, BLOCK_FAILURE]}
        loader={
          <div className="loading-indicator">
            <CircularProgress className="progress-success mt-15" size={70} />
          </div>
        }
        message="Processing"
      >
        <div className="app-main-container">
          <Sidebar
            sidebar={this.renderSidebar()}
            open={windowWidth <= 1199 ? navCollapsed : false}
            docked={windowWidth > 1199 ? !navCollapsed : false}
            pullRight={rtlLayout}
            onSetOpen={() => this.props.collapsedSidebarAction(false)}
            styles={{ content: { overflowY: '' } }}
            contentClassName={classnames({
              'app-conrainer-wrapper': miniSidebar
            })}
          >
            <div className="app-container">
              <div className="rct-app-content">
                <div className="app-header">{this.renderHeader()}</div>
                <div className="rct-page">{this.renderPage()}</div>
              </div>
            </div>
          </Sidebar>
          <ThemeOptions />
        </div>
      </ReduxBlockUi>
    );
  }
}

// map state to props
const mapStateToProps = ({ settings }) => ({ settings });

export default withRouter(
  connect(
    mapStateToProps,
    {
      collapsedSidebarAction,
      startUserTour
    }
  )(MainApp)
);
