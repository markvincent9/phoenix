/**
 * App Header
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import IconButton from '@material-ui/core/IconButton';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import screenfull from 'screenfull';
import Tooltip from '@material-ui/core/Tooltip';
import MenuIcon from '@material-ui/icons/Menu';
import { withRouter } from 'react-router-dom';

// actions
import { collapsedSidebarAction } from 'Actions';

// components
import Notifications from './Notifications';
import SearchForm from './SearchForm';
import QuickLinks from './QuickLinks';
import MobileSearchForm from './MobileSearchForm';

class Header extends Component {
  state = {
    isMobileSearchFormVisible: false
  };

  // function to change the state of collapsed sidebar
  onToggleNavCollapsed = () => {
    const val = !this.props.navCollapsed;
    this.props.collapsedSidebarAction(val);
  };

  // toggle screen full
  toggleScreenFull() {
    screenfull.toggle();
  }

  // mobile search form
  openMobileSearchForm() {
    this.setState({ isMobileSearchFormVisible: true });
  }

  render() {
    const { isMobileSearchFormVisible } = this.state;
    return (
      <AppBar position="static" className="rct-header">
        <Toolbar className="d-flex justify-content-between w-100 pl-0">
          <div className="d-flex align-items-center">
            <ul className="list-inline mb-0 navbar-left">
              {/* eslint-disable-next-line */}
              <li
                className="list-inline-item"
                onClick={e => this.onToggleNavCollapsed(e)}
              >
                <Tooltip title="Sidebar Toggle" placement="bottom">
                  <IconButton
                    color="inherit"
                    mini="true"
                    aria-label="Menu"
                    className="humburger p-0"
                  >
                    <MenuIcon />
                  </IconButton>
                </Tooltip>
              </li>
              <QuickLinks />
              <li className="list-inline-item search-icon d-inline-block">
                <SearchForm />
                <IconButton
                  mini="true"
                  className="search-icon-btn"
                  onClick={() => this.openMobileSearchForm()}
                >
                  <i className="zmdi zmdi-search" />
                </IconButton>
                <MobileSearchForm
                  isOpen={isMobileSearchFormVisible}
                  onClose={() =>
                    this.setState({ isMobileSearchFormVisible: false })
                  }
                />
              </li>
            </ul>
          </div>
          <ul className="navbar-right list-inline mb-0">
            <Notifications />
            <li className="list-inline-item">
              <Tooltip title="Full Screen" placement="bottom">
                <IconButton
                  aria-label="settings"
                  onClick={() => this.toggleScreenFull()}
                >
                  <i className="zmdi zmdi-crop-free" />
                </IconButton>
              </Tooltip>
            </li>
          </ul>
        </Toolbar>
      </AppBar>
    );
  }
}

// map state to props
const mapStateToProps = ({ settings }) => settings;

export default withRouter(
  connect(
    mapStateToProps,
    {
      collapsedSidebarAction
    }
  )(Header)
);
