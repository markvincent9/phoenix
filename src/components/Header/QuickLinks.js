/**
 * Quick Links
 */
import React from 'react';
import {
  Badge,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu
} from 'reactstrap';
import { Scrollbars } from 'react-custom-scrollbars';
import { Link, withRouter } from 'react-router-dom';
import Tooltip from '@material-ui/core/Tooltip';

// helpers
import { getAppLayout } from 'Helpers/helpers';

// intl messages
import IntlMessages from 'Util/IntlMessages';

const QuickLinks = ({ location }) => (
  <UncontrolledDropdown
    nav
    className="list-inline-item quciklink-dropdown tour-step-1"
  >
    <DropdownToggle nav className="header-icon p-0">
      <Tooltip title="Quick Links" placement="bottom">
        <i className="zmdi zmdi-apps" />
      </Tooltip>
    </DropdownToggle>
    <DropdownMenu>
      <Scrollbars
        className="rct-scroll"
        autoHeight
        autoHeightMin={100}
        autoHeightMax={350}
      >
        <div className="dropdown-content">
          <div className="dropdown-top d-flex justify-content-between rounded-top bg-primary">
            <span className="text-white font-weight-bold">Quick Links</span>
            <Badge color="warning">1 NEW</Badge>
          </div>
          <ul className="list-unstyled mb-0 dropdown-list">
            <li>
              <Link to={`/${getAppLayout(location)}/users/user-management`}>
                <i className="ti-user text-success mr-10" />
                <IntlMessages id="sidebar.userManagement" />
              </Link>
            </li>
          </ul>
        </div>
      </Scrollbars>
    </DropdownMenu>
  </UncontrolledDropdown>
);

export default withRouter(QuickLinks);
