FROM node:carbon-alpine

WORKDIR /usr/src/app
ADD . /usr/src/app

# RUN apt-get update -y \
    # && apt-get -y install curl python build-essential git ca-certificates

RUN npm install --silent

RUN npm run build

EXPOSE 3000

